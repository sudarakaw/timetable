module Slot exposing (Slot, fromString, ofDay)

-- TYPES


type Slot
    = First
    | Second
    | Third
    | Fourth
    | Fifth
    | Sixth
    | Seventh
    | Eighth
    | Ninth



-- PUBLIC HELPERS


ofDay : List Slot
ofDay =
    [ First, Second, Third, Fourth, Fifth, Sixth, Seventh, Eighth, Ninth ]


fromString : String -> Maybe Slot
fromString str =
    case str of
        "first" ->
            Just First

        "second" ->
            Just Second

        "third" ->
            Just Third

        "fourth" ->
            Just Fourth

        "fifth" ->
            Just Fifth

        "sixth" ->
            Just Sixth

        "seventh" ->
            Just Seventh

        "eighth" ->
            Just Eighth

        "ninth" ->
            Just Ninth

        _ ->
            Nothing
