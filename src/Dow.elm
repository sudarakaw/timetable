module Dow exposing (Dow, fromString, ofWeek, view)

import Html exposing (Html, h3, text)
import Html.Attributes exposing (class)



-- TYPES


type Dow
    = Mon
    | Tue
    | Wed
    | Thu
    | Fri



-- PUBLIC HELPERS


ofWeek : List Dow
ofWeek =
    [ Mon, Tue, Wed, Thu, Fri ]


fromString : String -> Maybe Dow
fromString str =
    case str of
        "mon" ->
            Just Mon

        "tue" ->
            Just Tue

        "wed" ->
            Just Wed

        "thu" ->
            Just Thu

        "fri" ->
            Just Fri

        _ ->
            Nothing



-- HELPERS


toString : Dow -> String
toString dow =
    case dow of
        Mon ->
            "Monday"

        Tue ->
            "Tuesday"

        Wed ->
            "Wednesday"

        Thu ->
            "Thursday"

        Fri ->
            "Friday"



-- VIEWS


view : Dow -> Html msg
view dow =
    h3 [ class "cell" ] [ dow |> toString |> text ]
