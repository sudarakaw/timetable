module Page.Home exposing (Model, Msg, init, session, update, view)

import AssocList as Dict exposing (Dict)
import Dow exposing (Dow)
import Html exposing (Html, div, p, text)
import Html.Attributes exposing (class)
import Http
import Json.Decode as Decode exposing (Decoder)
import Session exposing (Session)
import Slot exposing (Slot)



-- TYPES


type alias SubjectMap =
    Dict ( Dow, Slot ) String



-- MODEL


type Model
    = LoadingSubjects Session
    | HaveSubjects Session SubjectMap


init : Session -> ( Model, Cmd Msg )
init s =
    ( LoadingSubjects s, loadSubjects )



-- PUBLIC HELPERS


session : Model -> Session
session model =
    case model of
        LoadingSubjects s ->
            s

        HaveSubjects s _ ->
            s



-- UPDATE


type Msg
    = ReceivedSubjectsFromServer (Result Http.Error SubjectMap)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ReceivedSubjectsFromServer (Err _) ->
            ( HaveSubjects (model |> session) Dict.empty, Cmd.none )

        ReceivedSubjectsFromServer (Ok subjects) ->
            ( HaveSubjects (model |> session) subjects, Cmd.none )



-- HTTP


loadSubjects : Cmd Msg
loadSubjects =
    Http.get
        { url = "api/timetable.json"
        , expect = Http.expectJson ReceivedSubjectsFromServer decodeSubjects
        }



-- SERIALIZATION


toKeyedSubject : ( Maybe Dow, List ( Slot, String ) ) -> Maybe (List ( ( Dow, Slot ), String ))
toKeyedSubject ( maybeDow, slotSubjects ) =
    maybeDow
        |> Maybe.map
            (\dow ->
                slotSubjects |> List.map (\( slot, subject ) -> ( ( dow, slot ), subject ))
            )


decodeSubjects : Decoder SubjectMap
decodeSubjects =
    Decode.keyValuePairs decodeDay
        |> Decode.map (List.map (Tuple.mapFirst Dow.fromString))
        |> Decode.map (List.filterMap toKeyedSubject >> List.concat >> Dict.fromList)


toSlotSubject : ( Maybe Slot, String ) -> Maybe ( Slot, String )
toSlotSubject ( maybeSlot, v ) =
    maybeSlot |> Maybe.map (\slot -> ( slot, v ))


decodeDay : Decoder (List ( Slot, String ))
decodeDay =
    Decode.keyValuePairs Decode.string
        |> Decode.map (List.map (Tuple.mapFirst Slot.fromString))
        -- rearrange the resulting Tuple of (Maybe Slot, _) to a Maybe (Slot, _)
        -- & discard garbage data (slot names that fail to convert to valid
        -- Slot)
        |> Decode.map (List.filterMap toSlotSubject)



-- VIEW


view : Model -> ( String, Html msg )
view model =
    ( ""
    , case model of
        LoadingSubjects _ ->
            loadingView

        HaveSubjects _ subjects ->
            tableView subjects
    )


loadingView : Html msg
loadingView =
    div [ class "loading" ] [ p [] [ text "loading..." ] ]


tableView : SubjectMap -> Html msg
tableView subjects =
    div [ class "timetable" ]
        ((Dow.ofWeek |> List.map Dow.view)
            ++ (Dow.ofWeek |> dowSlotView subjects)
        )


dowSlotView : SubjectMap -> List Dow -> List (Html msg)
dowSlotView subjects =
    List.map
        (\dow ->
            div [ class "dow" ]
                (Slot.ofDay |> slotSubjectView dow subjects)
        )


slotSubjectView : Dow -> SubjectMap -> List Slot -> List (Html msg)
slotSubjectView dow subjects =
    List.map
        (\slot -> div [ class "cell" ] [ subjectView dow slot subjects ])


subjectView : Dow -> Slot -> SubjectMap -> Html msg
subjectView dow slot subjects =
    subjects
        |> Dict.get ( dow, slot )
        |> Maybe.map text
        |> Maybe.withDefault (text "")
