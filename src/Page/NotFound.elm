module Page.NotFound exposing (view)

import Html exposing (Html)


view : ( String, Html msg )
view =
    ( ""
    , Html.text "Page not found."
    )
