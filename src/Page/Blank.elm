module Page.Blank exposing (view)

import Html exposing (Html)


view : ( String, Html msg )
view =
    ( ""
    , Html.text ""
    )
