module Session exposing (Session, create, footerView, navKey, version)

import Browser.Navigation as Nav
import Html exposing (Html, a, footer, h6, p, text)
import Html.Attributes exposing (href, target)



-- TYPES


type Session
    = Session Nav.Key String



-- CONSTRUCTORS


create : Nav.Key -> String -> Session
create key version_string =
    Session key version_string



-- PUBLIC HELPERS


navKey : Session -> Nav.Key
navKey (Session key _) =
    key


version : Session -> String
version (Session _ version_string) =
    version_string



-- VIEWS


footerView : Session -> Html msg
footerView session =
    footer []
        [ h6 [] [ session |> version |> (++) "Timetable - " |> text ]
        , p []
            [ text "Copyright 2020 "
            , a [ href "https://sudaraka.org/", target "_newwin" ] [ text "Sudaraka Wijesinghe" ]
            , text "."
            ]
        , p []
            [ text "BSD 2-Clause license - "
            , a [ href "https://gitlab.com/sudarakaw/timetable", target "_newwin" ] [ text "source" ]
            , text "."
            ]
        ]
