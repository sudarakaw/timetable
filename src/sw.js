/* src/sw.js: serviceworker source
 *
 * Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

'use strict'

const CACHE_KEY = 'timetable'
    , CACHE_ID = `${CACHE_KEY}-${REVISION}`
    , APP_ROOT = '/timetable/'
    , ASSETS = [ APP_ROOT, ...serviceWorkerOption.assets ]

    , addToCache = (id, assets) => caches
        .open(id)
        .then(cache => cache.addAll(assets))
        .catch(console.error)

    , removeUnusedCaches = id => caches
        .keys()
        .then(keys => keys.forEach(key => {
          if(key.match(`${CACHE_KEY}-`) && CACHE_ID !== key) {
            return caches.delete(key)
          }
        }))

    , getFromNetwork = cache_id => request =>
        fetch(request)
          .then(response => {
            caches
              .open(cache_id)
              .then(cache => cache.add(request, response))

            return response.clone()
          })

    , getFromCache = cache_id => request =>
        caches
          .match(request)
          .then(cached_response => {
            if(cached_response) {
              return cached_response
            }

            return Promise.reject(new Error('not in cache'))
          })

    , fetchHandler = cache_id => request => {
        const url = new URL(request.url).pathname

        if(url.startsWith('api/', APP_ROOT.length)) {
          // Network first
          return getFromNetwork(cache_id)(request)
            .catch(() => getFromCache(cache_id)(request))
        }

        // Cache first
        return getFromCache(cache_id)(request)
          .catch(() => getFromNetwork(cache_id)(request))
      }


// SW install: cache all tracked assets
self.addEventListener
  ( 'install'
  , e => e.waitUntil(addToCache(CACHE_ID, ASSETS))
  )

// SW activate: remove all unused caches
self.addEventListener
  ( 'activate'
  , e => e.waitUntil(removeUnusedCaches(CACHE_ID))
  )

// SW fetch: respond from cache & use network as fallback
self.addEventListener
  ( 'fetch'
    , e => e.respondWith(fetchHandler(CACHE_ID)(e.request))
  )
