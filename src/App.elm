module App exposing (main)

-- src/App.elm: main module for the Elm application
--
-- Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY
-- This is free software, and you are welcome to redistribute it and/or modify
-- it under the terms of the BSD 2-clause License. See the LICENSE file for more
-- details.
--

import Browser
import Browser.Navigation as Nav
import Page.Blank
import Page.Home as Home
import Page.NotFound
import Route exposing (Route)
import Session exposing (Session)
import Url exposing (Url)



-- MODEL


type Model
    = Redirect Session
    | NotFound Session
    | Home Home.Model


init : Flags -> Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url navKey =
    url
        |> Route.fromUrl
        |> switchRoute (Redirect (Session.create navKey flags))



-- TYPES


type alias Flags =
    String



-- UPDATE


type Msg
    = UrlChanged Url
    | LinkClicked Browser.UrlRequest
    | HomePageSentMessage Home.Msg


toSession : Model -> Session
toSession model =
    case model of
        Redirect session ->
            session

        NotFound session ->
            session

        Home pageModel ->
            pageModel |> Home.session


switchRoute : Model -> Route -> ( Model, Cmd Msg )
switchRoute model route =
    let
        session =
            toSession model
    in
    case route of
        Route.Unhandled ->
            ( NotFound session, Cmd.none )

        Route.Root ->
            Home.init session
                |> applyWith Home HomePageSentMessage


applyWith : (model -> Model) -> (msg -> Msg) -> ( model, Cmd msg ) -> ( Model, Cmd Msg )
applyWith toModel toMsg ( pageModel, pageCmd ) =
    ( pageModel |> toModel, pageCmd |> Cmd.map toMsg )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model ) of
        ( UrlChanged url, _ ) ->
            url
                |> Route.fromUrl
                |> switchRoute model

        ( LinkClicked (Browser.Internal url), _ ) ->
            ( model
            , Nav.pushUrl (model |> toSession |> Session.navKey) (Url.toString url)
            )

        ( LinkClicked (Browser.External href), _ ) ->
            ( model, Nav.load href )

        ( HomePageSentMessage pageMsg, Home pageModel ) ->
            Home.update pageMsg pageModel
                |> Tuple.mapFirst Home
                |> Tuple.mapSecond (Cmd.map HomePageSentMessage)

        ( _, _ ) ->
            ( model, Cmd.none )



-- VIEW


view : Model -> Browser.Document Msg
view model =
    let
        ( title, content ) =
            case model of
                Redirect _ ->
                    Page.Blank.view

                NotFound _ ->
                    Page.NotFound.view

                Home pageModel ->
                    Home.view pageModel
    in
    { title =
        if String.isEmpty title then
            "Timetable"

        else
            title ++ " - Timetable"
    , body = [ content, model |> toSession |> Session.footerView ]
    }



-- MAIN


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        , subscriptions = \_ -> Sub.none
        , update = update
        , view = view
        }
