module Route exposing (Route(..), fromUrl)

-- src/Route.elm: Route parser
--
-- Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY
-- This is free software, and you are welcome to redistribute it and/or modify
-- it under the terms of the BSD 2-clause License. See the LICENSE file for more
-- details.
--

import Url exposing (Url)
import Url.Parser as Parser exposing (Parser, s)



-- TYPE


type Route
    = Unhandled
    | Root



-- CONSTRUCTOR


fromUrl : Url -> Route
fromUrl =
    Parser.parse routeMap >> Maybe.withDefault Unhandled



-- HELPERS


routeMap : Parser (Route -> a) a
routeMap =
    Parser.oneOf
        [ Parser.map Root (s "timetable")
        ]
