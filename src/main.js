/* src/main.js: entry point JS module for the app
 *
 * Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

'use strict'

import { Elm } from './App.elm'
import runtime from 'serviceworker-webpack5-plugin/lib/runtime';

import './style.sass'

const flags = VERSION || '0.0.0'

Elm.App.init( { flags } )

if('serviceWorker' in navigator) {
  runtime.register()
}

