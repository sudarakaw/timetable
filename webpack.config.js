/* webpack.config.js: Webpack configuration
 *
 * Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

const { resolve } = require('path')
    , Html = require('html-webpack-plugin')
    , Css = require('mini-css-extract-plugin')
    , Copy = require('copy-webpack-plugin')
    , SW = require('serviceworker-webpack5-plugin')
    , { DefinePlugin } = require('webpack')

module.exports = ({ VERSION, REVISION }, { mode }) => {
  const is_debug = 'production' !== mode

      , minify = is_debug ? false :
        { 'collapseBooleanAttributes': true
        , 'collapseWhitespace': true
        , 'decodeEntities': true
        , 'html5': true
        , 'minifyCSS': true
        , 'minifyJS': true
        , 'removeAttributeQuotes': true
        , 'removeComments': true
        , 'removeEmptyAttributes': true
        , 'removeRedundantAttributes': true
        , 'removeScriptTypeAttributes': true
        , 'removeStyleLinkTypeAttributes': true
        , 'useShortDoctype': true
        }

      , source =
        { 'html': resolve(__dirname, 'src/index.html')
        , 'js': resolve(__dirname, 'src/main.js')
        , 'sw': resolve(__dirname, 'src/sw.js')
        }

      , html_options =
        { 'template': source.html
        , minify
        }

      , copy_options =
        { 'patterns':
          [ { 'from': resolve(__dirname, 'assets/')
            , 'to': 'assets/'
            , 'globOptions': { 'ignore': [ '**/root/**' ] }
            }
          , { 'from': resolve(__dirname, 'assets/root/')
            }
          , { 'from': resolve(__dirname, 'api/')
            , 'to': 'api/'
            }
          ]
        }

      , config =
        { 'mode': is_debug ? 'development' : 'production'
        , 'entry': { 'assets/js/main.js': source.js }
        , 'output':
          { 'filename': '[name]'
          , 'publicPath': '/timetable/'
          }
        , 'module':
          { 'rules':
            [ { 'test': /\.elm$/
              , 'exclude': [ /elm-stuff/, /node_modules/ ]
              , 'use':
                [ { 'loader': 'elm-webpack-loader'
                  , 'options':
                    { 'debug': is_debug
                    , 'optimize': !is_debug
                    }
                  }
                ]
              }
            , { 'test': /\.(sa|sc|c)ss$/
              , 'use':
                [ Css.loader
                , 'css-loader'
                , 'sass-loader'
                ]
              }
            ]
          }
        , 'plugins':
          [ new Html(html_options)
          , new Css( { 'filename': 'assets/css/style.css' } )
          , new Copy(copy_options)
          , new SW
            ( { 'entry': source.sw
              , 'publicPath': ''
              }
            )
          , new DefinePlugin
            ( { 'VERSION': JSON.stringify(VERSION || '0.0.0')
              , 'REVISION': JSON.stringify(REVISION || '0000000')
              }
            )
          ]
        }

  return config
}
